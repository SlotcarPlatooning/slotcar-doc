%!TEX ROOT=slotcar-thesis.tex

\chapter{Control}
\label{chap:control}
We separate the control problem into (1) control of the velocity of the car and (2) control of the inter-vehicle distances. They are connected in the cascade structure, where the velocity control loop is the inner one and the distance control loop is the outer one, as seen in Fig.\ref{fig:block_diagram}.

\section{Velocity control}
The velocity measurement and the control loop is implemented in STM. The velocity controller is a standard discrete PI controller with following structure 
\begin{equation}
	C_\mathrm{v}(z) = k_{\mathrm{p},\mathrm{v}} + k_{\mathrm{i},\mathrm{v}} \frac{T_{\mathrm{s},\mathrm{v}}}{z-1} \text{, }
	\label{eq:vel_cont}
\end{equation}
where $k_{\mathrm{p},\mathrm{v}} = 2$, $k_{\mathrm{i},\mathrm{v}} = 10$ and sampling period $T_{\mathrm{s},\mathrm{v}} = \SI{0.005}{\second}$. The controller implements an anti-windup in a form of clamping $[-1, 1]$. The input control error $ e_v = v_{\mathrm{ref}} - v \;\text{, }$ where $ v_{\mathrm{ref}}$ is reference velocity. The control effort is the PWM duty cycle $D_\mathrm{c}$. The comparison of the simulation and the real implementation, Fig. \ref{fig:velocity_loop_test}, shows a good agreement in both, the velocity and the control effort.

For continuous analysis, we use the transfer function $$ \mathrm{C}_\mathrm{v}(s) = \frac{2s +10}{s} \text{.}$$ The bode diagram of the velocity loop is in Fig.~\ref{fig:speed_loop_bode}. 

\begin{figure}[!hb]
\centerline{
\includegraphics[scale=0.8,trim=5cm 10cm 5cm 11cm]{./fig/velocity_loop_bode.pdf}
\caption{The bode diagram of the velocity control loop.}
\label{fig:speed_loop_bode}
}
\end{figure}

\begin{figure}[!hb]
\centerline{
\includegraphics[scale=1]{./fig/v_model}
\caption{Comparison of speed controller in reality and in simulation. The PWM is the control effort.}
\label{fig:velocity_loop_test}
}
\end{figure}

\section{Distance control}

The platform aims for being able to run many forms of controllers. Therefore, we provide all kind of measurements (see Chap. \ref{chap:hw}), that can be used. The full states of the cars are also communicated among each other. We expect that especially the communication is the key factor for a successful controller. In this section, we provide some examples of standard controllers and some extended controller by communication. In all experiments, the leader is tracking user-defined velocity profile.


\subsection{Bidirectional control}
This experiment is a revision of an experiment, that was done previously done in \cite{lad}. We briefly describe the situation. We use a simple bidirectional control law, which accepts a single input---the weighted regulation error as in
\begin{equation}
	e = (d_i-d^\mathrm{ref}) + \epsilon (d_{i+1} - d^\mathrm{ref}), \label{eq:contLaw}
\end{equation}
where $d_i = x_{i-1}-x_{i}$ is the distance to the  ahead, $d_{i+1}=x_{i}-x_{i+1}$ is the distance to the car behind, $d^\mathrm{ref}$ is the reference distance the cars should keep among each other, and $\epsilon$ is a \textit{constant of asymmetry}. This constant weighs the contribution of the rear spacing error. When $\epsilon=0$, the controller only uses the distance to the car ahead (the so-called \textit{predecessor following}), when $\epsilon=1$, the car weighs the rear spacing error with the same weight as the front error (so called \textit{symmetric bidirectional control}).

In first experiment we wanted to track the reference distance. When leader moves with constant velocity, the $d_i$ is a ramp signal--In order to track a ramp signal double integrator in open-loop is needed. One integrator is already a part of the system from $v$ to $x$ and we added one integrator in controller. We selected a discrete PI controller
\begin{equation}
C(z) = k_\mathrm{p} + k_\mathrm{i} \frac{T_\mathrm{s}}{z-1} \text{, }
\label{eq:pi}
\end{equation}
where $k_\mathrm{p}$ resp. $k_\mathrm{i}$ is proportional resp. integral constant. The example of \textit{symmetric bidirectional control} is depicted in Fig. \ref{fig:pi_bid} and \textit{predecessor following} in Fig. \ref{fig:pi}. Which again proves the good agreement between the model and the reality (our platform). However, having another integrator in the open open loop means slow and oscillatory transient.

\begin{figure}
\centerline{
\includegraphics[max width=\textwidth]{./fig/pi}
\caption{Example of predecessor following ($\epsilon = 0$) with PI controller ($ k_\mathrm{p} = 3$, $k_\mathrm{i} = 0.8$, and $T_\mathrm{s} = 0.03$). Cars are tracking the reference distance $d^\mathrm{ref} = \SI{0.15}{\meter}$. The experiment expose higher over-shoot than simulation, because the platoon starts from zero velocity and the model neglects the Stribeck effect of the mechanical friction.}
\label{fig:pi}
}
\end{figure}

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/pi_bid}
\caption{An example of symmetric bidirectional control ($\epsilon = 1$) with PI controller, where $ k_\mathrm{p} = 2$, $k_\mathrm{i} = 2$, $T_\mathrm{s} = 0.03$.}
\label{fig:pi_bid}
}
\end{figure}

In the second experiment, we only required tracking of the leader's velocity and the steady-state distance error could be nonzero. Therefore, we needed only one integrator in open-loop (one integrator is already a part of the system). An instance of such a controller is the following filtered PD controller
\begin{equation}
C_d(z) = k_\mathrm{p} + k_\mathrm{d} \frac{N }{1 + N \frac{T_\mathrm{s}}{z-1}} \text{, }
\label{eq:pd}
\end{equation}
where $k_\mathrm{p}$ resp. $k_\mathrm{d}$ is proportional resp. derivative constant, $N$ is filter coefficient. In our experiments, we use the PD controller with parameters $k_\mathrm{p} = 5$,  $k_\mathrm{d} = 0.2$, $N = 50$, and $T_\mathrm{s} = 0.03$. The example using PD controller is depicted in Fig. \ref{fig:pd}.

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/pd}
\caption{An example of predecessor following ($\epsilon = 0$) with PD controller. We can see, that the controller alone is not able to track the desired distance $d_\mathrm{ref} = \SI{0.15}{\meter}$.}
\label{fig:pd}
}
\end{figure}

%\begin{figure}
%\centerline{
%\includegraphics[scale=1]{./fig/pd_best}
%\caption{}
%\label{fig:pd_best}
%}
%\end{figure}

The steady-state error was able to achieve by using the wireless communication, where leader send its velocity $v_0(t)$ to all the cars. The $v_0(t)$ was added as a feed-forward signal to the desired velocity produces by the distance controller.
The control law for the $i$th car becomes
 \begin{equation}
 	v_i^\mathrm{ref}(t) = v_0(t) + r_i(t), \label{eq:pd_ff}
 \end{equation}
where $r_i$ is the output of the distance controller (\ref{eq:pd}). We can see, that unlike the controller without communication (Fig.~\ref{fig:pd}, the same controller with augmented with communication (Fig.~\ref{fig:pd_bid_ff}) is able to track the reference distance. 

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/pd_bid_ff}
\caption{An example of symmetric bidirectional control ($\epsilon = 1$) with PD controller extend with the feed-forward from the speed of the leader $v_0$, because of which the cars are able to track the reference distance $d^{\mathrm{ref}} = \SI{0.15}{\meter}$.}
\label{fig:pd_bid_ff}
}
\end{figure}



\subsection{Cooperative adaptive cruise control}
\begin{figure}[!h]
\centerline{
\includegraphics[scale=1]{./fig/cacc_diagram}
\caption{The structure of CACC. Distance controller $C_d$ extended with feed-forwarded information of predecessor by using wireless communication.}
\label{fig:cacc_diagram}
}
\end{figure}
This example is similar to cooperative adaptive cruise control (CACC) as presented in~\cite{Milanes2014}. The structure, Fig.~\ref{fig:cacc_diagram}, shows that we augment our control loop with standard PD controller, as was previously used in \textit{predecessor following}, with time-headway and the target speed $v^\mathrm{ref}$ (obtained by wireless communication) of the predecessor. The preceding car-following policy has following structure \begin{equation}
P(s) = h s + 1 \text{, }
\label{eq:car-following_policy }
\end{equation} where $h$ is time-headway.
The result of such control can be seen in Fig~\ref{fig:cacc_pred_u}. Additionally, forwarding the target speed of the leader $v^{\mathrm{ref}_0}$ was tested; see~\ref{fig:cacc_leader}.


%some variations were tested, that the feed-forward is taken from: (1) the actual speed $v_{i-1}$; see \ref{fig:cacc_pred_v},  (2) . 


In these cases of CACC experiments, we can see, that the advantage of communication is in fast reaction time and, although not clearly shown, it allows tracking the target distance, which is where a standard PD fails. Moreover, forwarding the target speed of leader gives almost immediate response of other cars. Whereas in predecessor forwarding, it takes some time to propagate the change in target velocity of the leader. The simulations exposes, that forwarding the target speed of the predecessor gives no overshoot unlike forwarding target speed of the leader; however, not proved by experiments.



\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/cacc_pred_u}
\caption{An example of CACC; the \textbf{target speed of the predecessor} as the feed-forward. the Tracking distance $d^{\mathrm{ref}} = \SI{0.15}{\meter}$ and time-headway $h=0.03$.}
\label{fig:cacc_pred_u}
}
\end{figure}

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/cacc_leader}
\caption{An example of CACC; the \textbf{target speed of the leader} as the feed-forward. Tracking distance $d^{\mathrm{ref}} = \SI{0.15}{\meter}$ and time-headway $h=0.03$. Shows immediate response in velocity.}
\label{fig:cacc_leader}
}
\end{figure}

%\begin{figure}
%\centerline{
%\includegraphics[scale=1]{./fig/cacc_pred_v}
%\caption{An example of CACC; the \textbf{actual speed of the predecessor} as the feed-forward. the Tracking distance $d^{\mathrm{ref}} = \SI{0.15}{\meter}$ and time-headway $h=0.03$.}
%\label{fig:cacc_pred_v}
%}
%\end{figure}












