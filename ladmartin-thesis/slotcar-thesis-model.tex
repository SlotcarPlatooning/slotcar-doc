%!TEX ROOT=slotcar-thesis.tex

\chapter{Model}
\label{chap:model}
The vehicle is modeled as a loaded brush-type permanent magnet DC motor connected to the wheels through the gear train. The rotation of the wheels produces a force that pushes the car. The force is proportional to the torque of the wheels and also to the torque of the motor because we neglect slipping of the wheels and we consider the gear train as ideal. The car alone is represented by mass with weight $m$. The equations can be written down almost immediately using the bond graph in Fig. \ref{fig:bondGraph}


\begin{align}
	\frac{\mathrm{d}i(t)}{\mathrm{d}t}  &= \frac{u(t)}{L}  - \frac{R}{L} i(t) - \frac{k}{rnL} v(t) \label{eq:curDyn} \text{, }\\
	\frac{\mathrm{d}v(t)}{\mathrm{d}t} &= \frac{k}{mnr} i(t) - \frac{F_f(v)}{m} \label{eq:velDyn} \text{, }\\
	\frac{\mathrm{d}x(t)}{\mathrm{d}t} &= v(t) \label{eq:posDyn}.
\end{align}
The states are: the electrical current $i$ flowing through the motor winding (in $\si{\ampere}$), the velocity $v$ of the car (in $\si{\meter\per\second}$), and the traveled distance $x$ (in $\si{\meter}$). 

\begin{figure}[!hb]
	\begin{tikzpicture}[node distance = 1.5cm, font=\small]
		\node (Se)  {$S_e:u$};
		\node (J1_1) [right of=Se]{$1_i$};
		\node (I1) [below of=J1_1]{$I_1\!:L$};
		\node (R1) [above of=J1_1]{$R_1\!:R$};
		\node (G) [right of=J1_1]{$G:k$};
		\node (T1) [right of=G]{$T:n$};
		\node (T2) [right of=T1]{$T:r$};
		\node (J1_2) [right of=T2]{$1_v$};
		\node (I2) [right of=J1_2]{$I:m$};
		\node (R2) [below of=J1_2]{$R: F_\mathrm{f}(v)$};
		
		\bondCSEnd{Se}{J1_1}{}{}
		\bondCSEnd{J1_1}{I1}{}{}
		\bondCSStart{J1_1}{R1}{}{}
		\bondCSStart{J1_1}{G}{}{}		
		\bondCSEnd{G}{T1}{}{}
		\bondCSEnd{T1}{T2}{}{}
		\bondCSEnd{T2}{J1_2}{}{}		
		\bondCSEnd{J1_2}{I2}{}{}
		\bondCSStart{J1_2}{R2}{}{}
	\end{tikzpicture}
	\caption{Bond graph of the one-dimensional electromechanical dynamics of a slot-car as a loaded permanent magnet DC motor with a permanent magnet.}
	\label{fig:bondGraph}
\end{figure}

The control input is the voltage $u$ (in $\si{V}$). In reality, the input voltage comes in the Pulse Width Modulated (PWM) signal of frequency $\SI{40}{\kilo\hertz}$ with the duty cycle $D_\mathrm{c}$, where $D_\mathrm{c}\in[-1, 1]$. Hence $u(t) = U_\mathrm{r}\, D_\mathrm{c}(t)$ denotes just the low-frequency content, where $U_\mathrm{r}$ \footnote{The input (rail) voltage is not stabilized, therefore it can vary along the track because of changes in electrical resistance, although not much. It is good to make a dummy ride before running a comparison experiment just to find out how the voltage varies and then put the mean into the model.} (in $\si{V}$) is the rail voltage lowered by the input diode voltage ($0.8\si{V}$). 
The model has one significant non-linearity and it is the static friction. The friction force $$ F_f(v(t)) = b_\mathrm{d} v(t) + b_s \mathrm{sign}(v(t)) $$ applied on the car has two parts static and dynamic, where coefficient $b_\mathrm{d} $ is the dynamical (viscous) friction and $b_\mathrm{s} $ is the static friction\footnote{The model of friction is simplified; it is not correct for velocities around zero. For a better estimation of the friction, the Karnopp model should be used.}. 
The relevant physical parameters are in Table \ref{tab:params}. 

% TODO checknout jednotky
\begin{table}[ht]
	\begin{center}
	\caption{Parameters for the vehicle model.}\label{tab:params}  
	\begin{tabular}{@{}lcrl@{}}
	\toprule
		Physical parameter & Symbol & Value & Unit\\
		\midrule
		Resistance of the motor winding & $R$ & $8$ & $\si{\ohm}$ \\ 
		Inductance of the motor winding & $L$ & $2$ & $\si{\milli\henry}$ \\
		Torque constant & $k$ & $0.006$ & $\si{\newton\meter\per\ampere}$\\
		Mass of the slot car & $m$ & $0.15$ & $\si{\kilo\gram}$ \\
		Dynamic friction coefficient  & $b_\mathrm{d} $ & $0.27$ & $\si{\kilo\gram\per\second}$ \\
		Static friction coefficient & $b_\mathrm{s} $ & $0.53$ & $\si{\newton}$ \\
		Radius of the wheel & $r$ & $0.01$ & $\si{\meter}$ \\
		Gear train ratio & $n$ & $1/3$\\
		\bottomrule  
	\end{tabular}
	
	\end{center}
	
\end{table}

We implemented the full non-linear model in MATLAB Simulink, see Fig\;\ref{matlab_model}, which is used for simulations. We can see in the figure \ref{fig:model_free} a comparison of the simulation and the reality. The input for this test was the PWM duty cycle from $0.1\%$ to $0.5\%$. We can see that velocity and of course the back EMF (from which the velocity is calculated) fit. Therefore the model of non-linear friction is accurate. It needs to be noted, that it only works once the wheels are rotating as you can also see from the figure. It turns out that the model of static friction is more complicated when starting from zero velocity, because on the way up $0.1\%$ was not enough to move the wheels and it was enough on the way down; it has a hysteresis.

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/model_free}
\caption{Car model: the reality compared to the simulation. In this case the car is lifted and the wheels are rotating in the air. Therefore the coefficients are different $m = 0.01$, $b_\mathrm{d}  = 0.01$, $b_\mathrm{s}  = 0.09.$}
\label{fig:model_free}
}
\end{figure}

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/model}
\caption{Car model: the reality compared to the simulation.}
\label{fig:model}
}
\end{figure}

\begin{figure}[!t]
\centerline{
\includegraphics[scale=0.8]{./fig/matlab_model.pdf}
\caption{The Simulink model of the car. In the case of the car $k_\mathrm{e} = k_\mathrm{t} = k$. }
\label{matlab_model}
}
\end{figure}



\section{Linearization}

Note that while the linear model turns out nearly perfect for most aspects here, it fails badly when describing the friction phenomenon. Here the friction comes from three sources---friction induced by the angular motion of the rotor shaft, friction in the slot, and the rolling friction. It is well known that the rolling friction does not depend on the velocity but only depends on the normal force (here it is not only the weight but also the attractive force of magnets that push the slot car against the track). Introducing some nonlinearity into the model seems inevitable. But temporarily, the linear model of a friction is used to get a transfer function as rough models of the overall dynamics.

The electric current dynamics is very fast compared to the mechanical dynamics of the velocity, so we can neglect it by setting $\frac{\mathrm{d}i(t)}{\mathrm{d}t} = 0$ in (\ref{eq:curDyn}). Separating $i(t)$ and plugging it to (\ref{eq:velDyn}), we get 
\begin{align}
	\frac{\mathrm{d}v(t)}{\mathrm{d}t} &= \frac{U_\mathrm{r} k}{Rmnr} d(t) - \frac{b}{m}v(t) - \frac{k^2}{Rmr^2n^2} v(t)\label{eq:velDyn2} \text{, } \\
	\frac{\mathrm{d}x(t)}{\mathrm{d}t} &= v(t) \label{eq:posDyn2}.
\end{align}

Transfer functions from the input voltage are
\begin{align}
	G(s) &= \frac{v(s)}{D(s)}=\frac{\frac{k U_\mathrm{r}}{Rmnr}}{s+\frac{b}{m} + \frac{k^2}{Rmr^2n^2}} \text{, }\\
	H(s) &= \frac{x(s)}{D(s)} = \frac{1}{s} G(s).
\end{align}
After plugging in the values of the parameters,
\begin{equation}
	G(s) = \frac{3.2}{0.3s + 1}.
\end{equation}

In combination with a \textit{dead-zone} on input, which represents the static friction, is the linear model good approximation of reality. The dead-zone is set to range $[-0.30, 0.30] \%$ of PWM duty cycle, which is needed to overcome the static friction.

