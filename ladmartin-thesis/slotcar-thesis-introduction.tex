%!TEX ROOT=slotcar-thesis.tex
\chapter{Introduction}
\label{chap:intro}

\begin{figure}
\includegraphics[max width=\textwidth]{./fig/draha.jpg}
\caption{An example of a fraction of our platoon of vehicles.}
\label{fig:platoon}
\end{figure}

Distributed control of platoons of vehicles has been an active research topic for a few decades. Recently, the topic got more popular even in public thanks to driver-less cars. The evolution can be track from cruise control (CC) through adaptive cruise control (ACC) to cooperative ACC (CACC). The cooperation is that a car shares some information with others using wireless communication. An implementation and an experimental evaluation of a CACC is one part of this thesis. CACC is a proved concept and it was tested experimentally, for example, in~\cite{Milanes2014} or in \cite{Naus2010}. Other examples also tested withing this work are \textit{predecessor following}~\cite{Seiler2004a}, \textit{symmetric bidirectional control}~\cite{Barooah2005}.

The motivations for creating the platform are following. Having a platoon of real vehicles can be expensive, and even more expensive when an accident happens. It also requires a lot of space. Our platform is small, affordable and collisions are just moments for laughter. The platform (see an example in Fig.~\ref{fig:platoon}) has been presented at school "open doors" events, at the Gaudeamus, and at the Long Night of Museums. From our experience, it is very attractive; we had many interested visitors, and children.

The platform, as seen in Fig.~\ref{fig:overallScheme} and Fig.~\ref{fig:block_diagram}, is based on racing slot cars 1:32 produced by \textit{Carrera} used commonly as toys by children and adults. Originally, these slot cars had no onboard controller. They are also free of any sensor or communication interface. Their velocity is controlled remotely by a human player varying the voltage on the conducting strips on the track. The slot cars have been upgraded significantly by equipping them with custom-made electronics including powerful microprocessors, sensors, and a communication interface. The main computing unit is the popular the Raspberry Pi Compute Module, which runs Debian Linux. All these parts still fit within the small car, and the appearance is (almost) unchanged. A part of the platform is a user PC running GUI and MATLAB Simulink. 



\begin{figure}[] 
\begin{tikzpicture}[auto,node distance=0.5cm, line width=1pt, >=latex]
	\node[block] (gui) { GUI};
	\node[block] (matlab) [left=of gui] {MATLAB};
\draw ([xshift=-5pt, yshift=-5pt]matlab.south west) rectangle ([xshift=5pt, yshift=5pt]gui.north east) node [above, xshift=-1.8cm] {PC};
\draw[<->] (gui) -- (matlab);
	\node[leader, solid] (v1) [right= 1.5cm of gui] {$1$};
	\node[vehicle] (v2) [right= of v1] {$2$};
	\draw[<->] (v2) -- (v1);
	\node (dd) [right=0.5cm of v2] {\ldots};
	\draw[<->] (v2) -- (dd);
	\node[vehicle] (vN) [right=0.5cm of dd] {$N$};
	\draw[<->] (dd) -- (vN);
	\node [cloud, draw,cloud puffs=10,cloud puff arc=120, aspect=2, inner ysep=1em] (wifi) [above =0.5cm of v1] {Wi-Fi};
	
	
	\draw[thick] ([yshift=-1pt]v1.south west) -- ([yshift=-1pt]vN.south east);
	\begin{pgfonlayer}{bg}
		\draw [fill=black!20!white]([yshift=-5pt, xshift=-5pt]v1.south west) rectangle ([yshift=+5pt, 			xshift=5pt]vN.north east) node [above, xshift=-2.0cm] {Track};
	\end{pgfonlayer}
	
	\path (gui.90)  edge [bend left, bend angle=45, <->] (wifi.180);
	\path (v1.90)  edge [bend left, bend angle=45, <->] (wifi.270);
	\path (v2.90)  edge [bend right, <->] (wifi.320);
	\path (vN.90)  edge [bend right, bend angle=45, <->] (wifi.360);
	
	
	
\end{tikzpicture}
\caption{A visualization of the platform.}
\label{fig:overallScheme}
\end{figure}

\begin{figure}[]
\centerline{
\includegraphics[scale=1]{./fig/block_diagram}
\caption{A block diagram showing the $k$-th car in the context of the platform.}
\label{fig:block_diagram}
}
\end{figure}

\paragraph{The main features of the platform} 
\begin{itemize}
  \item Small size: each car is $\SI{13}{\centi\meter}$ in length and $\SI{5.5}{\centi\meter}$ in height. The simplest required track for experiments is a circle with diameter of $\SI{1}{\meter}$; it easily fits on an office desk.
  \item Low cost: the total cost of each autonomous car is approximately 300\,\euro, including the bare slot car itself. A basic track costs about 70\,\euro.
  \item Durability: since all the electronics is located inside the plastic cover of the slot car, it can easily withstand almost any crash between cars.
  \item Ease of controllers design: a controller is just a plugin for the main application in the car. Hence, it can be easily developed, deployed and run. Moreover, source codes for the most commonly used controllers (PI, PD, \ldots) are already provided.  
  \item Graphical application for the platoon setup: a Java application is provided for the platoon management. It allows a user to upload controllers to the cars, select a controller, and set its parameters, as well as update the firmware of cars remotely.
  \item Matlab interface: the data measured by each car are communicated among other cars, as well as to GUI. Matlab can be used not only to visualize and analyze the data, it can also be used to change reference values of the platoon, such as the target velocity of leading vehicle.
\end{itemize}   

\paragraph{The choice of Raspberry Pi Compute Module}
From the hardware point of view, the Raspberry Pi Compute Module (CM) was chosen because of its small size, performance and huge community of developers. It became sort-of a standard in embedded technology.

CM runs a operating system(OS)---a Linux distribution called \textit{Raspbian} (based on the popular Debian distribution, as the name reflects). Having an operating system allows a user to use many convenient services such as SSH and SCP for easy maintenance, debugging, and file management. While having already implemented drivers for Wi-Fi, SPI, and other standard peripherals, a developer can only focus on a application itself.

Although we do not use a real-time version of Linux, the OS is capable of running the control tasks with a sampling period of $\SI{30}{\milli\second}$.       





\section{The thesis in context of history of the project}
A previous version of the experimental platform has already been reported in \cite{martinec_vehicular_2012}. An overview of the current version of the platform has also been published \cite{lad}. This thesis is a logical follower, where the platform is described in more detail and should serve as a manual for future contributors. It is also a summary of what the platform is capable of. The platform is still in development; however, it is already able to serve its purpose. Some examples of control principles were successfully tested; see Chapter \ref{chap:control}.

%TODO popsat jednotlive verze projektu
The platform is being developed within numerous student projects following the spirit of \textit{open source} and \textit{open hardware}. The code and the schematics are publicly available through a git repository \url{https://gitlab.fel.cvut.cz/SlotcarPlatooning}. Noticeable contributors or previous work \footnote{Before the git repository was created.} are Jan Moravec, who designed distance sensors and proposed principle of measuring the distance; a revision of the code has been done recently. A group, consisting of me, Filip Svoboda and Filip Richter, designed electronics of the cars. The principle of speed measurement was presented in \cite{bclad}. Other contributors are listed in our paper. It is not a short-term project for one person for one semester. The recent work has been focused on bug-fixing, cleaning of the code, and experiments. The project is also published at \cite{hackaday}. This is a popularization website allowing you to create a blog about a project. The website is focused on technical, mainly home-made, projects. The community consists of engineers and engineering enthusiasts.

\paragraph{Git repositories description:}
\begin{description}
	\item [slotcar-sw] Raspberry Pi application and GUI, both written in Java.
	\item [slotcar-fw] Firmware for microcontroller STM32F401, written in C.
	\item [slotcar-fs] Raspberry Pi file system and OS related things.
	\item [slotcar-hw] Hardware schematics, drawn in Eagle.
	\item [slotcar-ctrls] Distance controllers, written in Java.
	\item [slotcar-doc] Papers, documentation, and datasheets.
\end{description}

\section{Content}
This thesis is structured as follows.\\
\textbf{Chapter \ref{chap:hw}} Describes hardware of a single car and the principles of measurements.\\
\textbf{Chapter \ref{chap:sw}} Describes the software running on Raspberry Pi, firmware running on a microcomputer, GUI, and Slotcar MATLAB Simulink library. The microcomputer is responsible for measurements and control of velocity of the car.\\
\textbf{Chapter \ref{chap:user_manual}} Shows how to work with the platform from a developer's perspective, and how to maintain the platform.\\
\textbf{Chapter \ref{chap:model}} Describes the mathematical model of the car and show some experiments, that prove correctness. \\
\textbf{Chapter \ref{chap:control}} Presents some control algorithms; the simulation and the real experiment, and compares it.\\
\textbf{Chapter \ref{chap:conclusion}} Concludes the results of the work.
