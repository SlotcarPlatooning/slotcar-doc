%!TEX ROOT=slotcar-thesis.tex
\chapter{Hardware}
\label{chap:hw}
In this chapter, we describe hardware solution and electronics. See Fig.~\ref{fig:slotcar_hw}.

\paragraph{Mechanics}
The drivetrain is very simple: a brushed DC motor with a permanent magnet drives the rear axle through a gear train (no differential) with 3:1 ratio. The car drives on the track in a guiding slot, which allows the car to move only forward or backward. The car takes electricity from the pair of powered metal strips placed along the guiding slot. Since the side motion is severely restricted, the platform is only suitable for experiments related to longitudinal dynamics and control.  

\paragraph{Electronics}
It consists of two boards: \textit{top} and \textit{bottom}. Additionally, there are two distance sensors and one infrared encoder (IRC) used for speed measurement. All schematics of printed circuit board (PCB) are held in the slotcar-hw repository. 
For design, we used the software named Eagle which is widely used and very user-friendly.

\paragraph{Top board}
On the \textit{top} board, there is a Raspberry Pi Compute Module (RPI), which is the main computation unit. Next, there is a USB connector in which a Wi-Fi dongle is plugged in. A Wi-Fi network is used as the main communication interface among all the cars and GUI. We also equipped the cars with the Zig-Bee communication interface \footnote{It is not currently used. We prefer Wi-Fi because it is more common communication interface.}.

\paragraph{Bottom board containing low-level electronics.} Namely power supply chain (rail voltage $U_{\mathrm{r}}$; $\SI{5}{\volt}$; $\SI{3.3}{\volt}$; $\SI{1.8}{\volt}$), a motor driver, the microcontroller STM32F401~(STM)~\cite{stm32f401rb}, a sensor for acceleration and rotation, a magnetometer and a super capacitor. IRC and the distance sensors are connected to the \textit{bottom} board by cables.

\begin{figure}
\includegraphics[scale=0.5]{./fig/car_hw.pdf}
\caption{The hardware of the car; the bottom board, the top board, and the car.}
\label{fig:car_hw}
\end{figure}


\begin{figure}
\includegraphics[max width=\textwidth]{./fig/hw_structure.pdf}
\caption{Schematic diagram of the hardware.}
\label{fig:slotcar_hw}
\end{figure}

\section{Motor driver and back-EMF measurement}
\label{motor}

\begin{figure}
\includegraphics[max width=\textwidth]{./fig/motor_driver_measurements.pdf}
\caption{Schematic diagram of the interconnection of STM, the driver and the motor.}
\label{fig:motor_driver_measurement}
\end{figure}

The motor is powered from the rail voltage and is driven by STM through the H-bridge motor driver DRV8816 \cite{drv8816}. The whole situation is depicted in Fig.~\ref{fig:motor_driver_measurement}.

\paragraph{H-Bridge}
DRV8816 is not an actual motor driver, but an extended full H-bridge. Among standard functions of a standard H-bridge, it provides a dead-time check and a coast mode. In the coast mode, output pins are set to a high-impedance state, which is essential for back-EMF measurements.

\paragraph{Lock Anti-Phase mode}
The motor is driven by two signals A and B. Each signal drives one-half of the H-bridge (the high level = the top transistor is on, the bottom is off and vice versa). There are several ways of driving a motor. Some are explained in \cite{h-bridge-secrets}. We chose the \textit{lock anti-phase} because, among others, its behavior is linear. In other words, the shape of acceleration of the motor is symmetrical to the shape of braking.

This mode is periodically switching between a \textit{forward} and \textit{reverse} state with a high frequency (tens of $\si{\kilo\hertz}$); see Fig.~\ref{fig:lock_anti_phase}. In average, the proportion of staying in one of these states determines whether the motor is rotating forward or backward and how fast. When the proportion is 1:1, the motor stops.

\begin{figure}[ht]
\centering
\begin{circuitikz}[scale=1]
\draw (0,0) node[nigfetd] (T1) {};
\draw (4,0) node[nigfetd, xscale=-1] (T3) {};
\draw (0,-2) node[nigfetd] (T2) {};
\draw (4,-2) node[nigfetd, xscale=-1] (T4){};
\draw (T1.D) -- (T3.D);
\draw (T2.S)-- ++(0,-0.1) -- ($(T4.S)+(0,-0.1)$) -- (T4.S);
\draw (T1.S) -- (T2.D);
\draw (T3.S) -- (T4.D);
\draw ($(T1.S)+(0,-0.3)$) to[european,motor,*-*] ($(T3.S)+(0,-0.3)$);
\draw  (T1.S) -- ++(0.5,0) to[D*,name=D1,l_=D1] ($(T1.D)+(0.5,-0.1)$)-- ++(-0.5,0);
\draw  (T2.S) -- ++(0.5,0) to[D*,name=D2,l_=D2] ($(T2.D)+(0.5,-0.1)$)-- ++(-0.5,0);
\draw  (T3.S) -- ++(-0.5,0) to[D*,name=D3,l=D3] ($(T3.D)+(-0.5,-0.1)$)-- ++(0.5,0);
\draw  (T4.S) -- ++(-0.5,0) to[D*,name=D4,l=D4] ($(T4.D)+(-0.5,-0.1)$)-- ++(0.5,0);
\draw ($(T2.S) + (2,-0.1)$) --  node[rground] (GND) {} ($(T2.S) + (2,-0.1)$);
\draw ($(T1.D) + (2,0)$) --  node[sground, yscale=-1] (VCC) {} ($(T1.D) + (2,0)$);

\draw ($(T1)+(-0.5,0)$) node[left]{Q1};
\draw ($(T2)+(-0.5,0)$) node[left]{Q2};
\draw ($(T3)+(+1.3,0)$) node[left]{Q3};
\draw ($(T4)+(+1.3,0)$) node[left]{Q4};

\draw [red,ultra thick,dashed]  ($(VCC)+(0,0.2)$) .. controls (VCC) and ($(T1)+(-0.5,1)$)  .. ($(T1)+(-0.5,0)$);
\draw [red, ultra thick,dashed]  ($(T1)+(-0.5,0)$) .. controls ($(T1)+(-0.5,-2)$) and ($(T4)+(0.5,2)$) .. ($(T4)+(+0.5,0)$);
\draw [red,ultra thick,dashed,-latex]  ($(T4)+(+0.5,0)$) .. controls ($(T4)+(0.5,-1)$) and ($(GND)+(0.2,0)$)  .. ($(GND)+(0,-0.3)$);

\draw [blue,ultra thick,dashed]  ($(VCC)+(0,0.2)$) .. controls (VCC) and ($(T3)+(0.5,1)$)  .. ($(T3)+(0.5,0)$);
\draw [blue,ultra thick,dashed]  ($(T3)+(0.5,0)$) .. controls ($(T3)+(0.5,-2.1)$) and ($(T2)+(-0.5,2)$) .. ($(T2)+(-0.5,0)$);
\draw [blue,ultra thick,dashed, -latex]  ($(T2)+(-0.5,0)$) .. controls ($(T2)+(-0.5,-1)$) and ($(GND)+(-0.2,0)$)  .. ($(GND)+(-0.0,-0.3)$);

%\draw [blue,ultra thick,dashed,->]  (D3) .. controls ($(D3)+(0,1)$) and (VCC)   .. ($(VCC)+(0,1)$);
%\draw [blue,ultra thick,dashed]  (D2) .. controls ($(D2)+(0,2)$) and ($(D3)+(0,-2)$) .. (D3);
%\draw [blue,ultra thick,dashed]  (D2) .. controls ($(D2)+(0,-1)$) and (GND)  .. ($(GND)+(0,-0.5)$);

%\draw [green,ultra thick,dashed]  (T2.D) .. controls ($(VCC) + (0,-1.5)$) .. (T4.D);
%\draw [green,ultra thick,dashed]  (T2.S) .. controls ($(GND) + (0,-0.5)$) .. (T4.S);
%\draw [green,ultra thick,dashed]  (T2.S) .. controls ($(T2) + (-1,0)$) .\textbf{•}. (T2.D);
%\draw [green,ultra thick,dashed]  (T4.D) .. controls ($(T4) + (1,0)$) .. (T4.S);

\end{circuitikz}
\caption{In the picture we can see an H-bridge and flows of electric current. In the \textit{forward} resp. \textit{reverse} state, the transistors (Q1, Q4) resp. (Q2, Q3)  are open, and (Q2, Q3) resp. (Q1, Q4) are closed. The red resp. blue line shows the flow of the electrical current through the motor.}
\label{fig:lock_anti_phase}
\end{figure}

\paragraph{PWM}
We control the motor by generating a complementary Pulse Width Modulation (PWM) signal, which means, that one side of the H-bridge is controlled by one PWM signal and the other side is controlled by its inversion. The duty cycle of PWM $$ d_\mathrm{c} = \frac{t_{\mathrm{on}}}{t_{\mathrm{on}} + t_{\mathrm{off}}}, $$ where $t_{\mathrm{on}}$ resp. $t_{\mathrm{off}}$ is time of high resp. low state of the signal.

There is a convention problem. In the \textit{lock anti-phase} mode, when $d_\mathrm{c} = 50\%$, the motor stops. The average input voltage on the motor is defined as $$ u_{\mathrm{avg}} = U_\mathrm{r} \frac{t_{\mathrm{on}} - t_{\mathrm{off}}}{t_{\mathrm{on}} + t_{\mathrm{off}}} = U_\mathrm{r} D_\mathrm{c} = U_\mathrm{r} ( 2 d_\mathrm{c} - 1 ), $$ where $U_\mathrm{r}$ is the rail voltage and $D_c$ is the duty cycle of our linear model. The conversion formula is $$ d_\mathrm{c} = \frac{D_\mathrm{c} + 1}{2}.$$

\begin{figure}
\includegraphics[max width=\textwidth]{./fig/pwm_center_mode.pdf}
\caption{PWM center-align mode. The timer is counting (CNT) up/down from MIN to MAX, toggles PWM on equal comparison with COMP value.}
\label{fig:pwm_center_align}
\end{figure}
A timer which generates PWM is set to a center-align mode (Fig.~\ref{fig:pwm_center_align}). In this mode, the timer counts repeatedly up to a maximum value and then down to zero. It toggles a output pin on a comparison with a preset level. The preset level determines the duty cycle and the maximum value determines the frequency of PWM. The timer generates an interrupt once it reaches zero or the maximum value. Therefore, interrupts appear twice in one period of PWM.

\paragraph{Back-EMF (Back-induced ElectroMotive Force) measurement}
We switch the motor driver to the coast mode, and the motor is floating unconnected to the driver. When the electric current decays in the motor winding, the motor generates only the back-EMF, and then we measure it; see Fig.~\ref{fig:bemf1}. The time window, in which the feasible range of electric current is able to decay, is determined experimentally to $\SI{300}{\micro\second}$. After that sequence, we switch the motor back to the normal operation. The voltage on the motor is measured by using a differential amplifier and by using the integrated analog-to-digital converter (ADC) on STM.

\begin{figure}
\centerline{
\includegraphics[scale=1]{./fig/bemf1.pdf}
}
\caption{Two measurements of back-EMF for two different duty cycles.}
\label{fig:bemf1}
\end{figure}

\paragraph{Electric current measurement}
We also measure the electric current flowing through the motor by using an another differential amplifier which measures a voltage drop on a resistor $\SI{0.2}{\ohm}$. The measurement is synchronized with PWM, and we measure it only once per period at the longer state of PWM, because we do not want to measure any noise factors connected with PWM level changes. Mainly, it is important when the duty cycle is very low or very high.
%TODO měření proudu však nefunguje správně s přídavnou horní deskou, pravděpodobně, je zde nějaký přeslech.

\section{Velocity measurement}
The translation velocity is measured by using two types of sensors. The considered velocity range is between $\SI{-2}{\meter\per\second}$ and $\SI{2}{\meter\per\second}$. At higher velocities, cars can get off the track in turns. The first sensor is a standard (albeit home-made) incremental rotary encoder (IRC). The other one is based on measuring the back-EMF. Since we have two measurements, each with a different precision and accuracy, they are be fused using an estimator, namely Kalman filter (see \cite{bclad}). This filter also provides an estimate of the friction force affecting movement of cars.

\subsection{IRC}
A binary (black and white) disk is attached to the rear axle. The infra-red emitter/receiver QRE1113 (see \cite{qre1113}) is pointed to the disk, and by measuring the intensity of reflected light, is determined the color in front of the sensor. There are two standard ways how to determine the speed of rotation. First, by measuring the frequency of changes of the color in front of the sensor. Second, by measuring the time between these changes.

\paragraph{Measurement of the time between changes}
The reason why we use this method is that we have only six ( three black and three white) stripes which means six changes per one turn. Moreover, the sensor is situated after the gear train from the perspective of the motor. Therefore the angular speed is divided and becomes too slow for measuring just the frequency, even at higher velocities.

The speed equation in a time of the color change $k$ is following 
\begin{equation} 
	v(k) = \frac{4 \pi r}{A} \frac{1}{\tau(k)} \text{  ,}
\end{equation}
where $r$ is the radius of the wheel, $A$ is the number of color stripes and $\tau$ is the time between the recent and the last color change.
In our case, we measure the time by a timer
\begin{equation}
\tau(k) = N(k) \frac{1}{f_\text{vf}}
\end{equation}
where $f_{vf}$ is frequency of the timer,  and $N$ is number of ticks of the timer, which is reset after each change of the color. In fact, the sensor can measure only the speed of rotation and not its direction. The information about direction is borrowed from the back-EMF measurement.

\subsection{Velocity from Back-EMF}
The back-EMF, denoted $u_{bemf}$, is proportional to the motor angular speed as \\\begin{equation}
u_{\mathrm{bemf}}(t) = k \omega(t) \text{,}
\end{equation} where $k$ is the so-called back-EMF constant (in SI units identical to the motor torque constant).  The equation for obtaining the speed is following \begin{equation} v_\mathrm{e}(t) = \frac{rn}{k} \: u_{\mathrm{bemf}}(t) \text{    ,}  \end{equation} where $n$ is gear ratio and $r$ is the radius of wheel of the car. Unlike IRC, the back-EMF measurement gives the direction of rotation. The con of this method is that measuring back-emf is very complicated. However, once you have the value, it is directly proportional to the velocity.

\section{Distance measurement}
We could not find any commercially available sensor which would satisfy the following requirements:
\begin{itemize}
  \item small size, such that it fits into the car,
  \item wide field of view (important when going through turnings),
  \item no interference between the front and rear distance sensor.
\end{itemize} 
It was possible to satisfy the first two requirements; however, the third one seemed like a big obstacle. That is why we designed own sensor. It is based on an infrared (IR) LED and a phototransistor. The diode emits square pulses with a given frequency, and the phototransistor receives the signal. To get rid of disturbances such as the sunshine or the room lighting, the demodulation of the received signal exploits the correlation (synchronous detection)---the frequency of the transmitted signal is known. There are distinct frequencies for both front and the rear distance measurements: for the front it is $f_\mathrm{f}=1111\,\si{Hz}$ and for rear it is $f_\mathrm{r}=1666\,\si{Hz}$. Both are chosen to be sufficiently well separated and also not being the multiplier (higher harmonics) of $100\,\si{Hz}$, which is a frequency of the fluorescent tubes used in the lab.

\paragraph{We do not use reflections.}
Reflection-based sensors are limited by the angle of reflections. In our case, each car receives the emitted signal by its neighbors (front and rear). Since we are using wide angle IR LEDs and phototransistors, it gives a good precision even in turnings. The sketch of the distance measurement is in Fig.~\ref{fig:distMeas}. 

\paragraph{Measurement principle.}
The car with index $i-1$ emits the signal from its rear LED with the frequency $f_\mathrm{f}$. The car $i$ detects this signal at its front transistor and demodulates it. The distance $d$ is calculated from the strength $s$ of the demodulated signal by \begin{equation}
    d(t) =  \sqrt{\frac{c}{s(t)}} \text{, }
\end{equation} where $c$ is a constant derived from calibration. The range of the sensor is approximately from $5\si{cm}$ to $50\si{cm}$. Similarly, the car $i-1$ calculates its distance to the car $i$. Compared to the distance measurement schemes based on reflections (some commercially available hobby-grade distance sensors rely on it), the proposed scheme achieves four times stronger signal (the distance for the light to travel is halved). 

\begin{figure}[ht] 
\centering
\includegraphics[scale=1]{./fig/distance_sensor.pdf}
\caption{Schematic of distance measurement between two cars. Two neighbors $\mathrm{car}_{i-1}$ and $\mathrm{car}_{i}$ exchange IR signals (with different frequencies). From intensities of the signals, the cars calculate the distance. In turnings (red color), the sensors are not in parallel position; each signal has slightly different time of fly. Therefore, the intensities differ and also the measured distances are different.}
\label{fig:distMeas}
\end{figure} 

\paragraph{As any other sensors, these also have some disadvantages}
\begin{enumerate}
\item Sensors require calibration procedure. Currently, we calibrate the constant $c$ by telling the sensor what is the real distance and by comparing it to measurement and $c$ is readjusted.
\item It is necessary to have a car ahead, transmitting the modulated signal. However, this does not cause any problems in our setting, since there is always a platoon leader. The leader drives independently, following the desired speed profile. Hence, the leader itself does not need any front distance measurement.
\item In turnings, bumpers of two neighbors are not parallel (see Fig.~\ref{fig:distMeas}); therefore, the distances that measure car $i$ and $i-1$ are different. It cause problems for bidirectional control. We avoided this by using the communication between two neighbors. By sharing the information, the predecessor takes the front distance of the successor car as its rear distance. In future work, the car should combine the information of its measurement and of measurement of its successor.
\end{enumerate}

\section{Super capacitor}
The main problem with the RPI is its power consumption. It is necessary to have a backup power source because the car, for example during crashes, may lose the connection with the power line. The loss of power leads to shutdown of RPI, which cause a loss of data on the one hand and an additional time needed for reboot on the other hand. The backup is something that saves time, since the boot time is about a minute long. Of course, repairing harmed OS image requires much more.

Therefore, we added a supercapacitor ($\SI{1}{\farad}$; $\SI{5}{\volt}$) which serves as a power backup for $ \leq \SI{5}{\volt}$ branches. It is able to backup the circuit for approximately $\SI{5}{\second}$, which seems to be enough for emergency situations, such as crashes. Note that since the car has a significant power consumption, a regular capacitor is not sufficient for this task. Adding a battery is also not sufficient, because it needs a regular maintenance.

\section{Accelerometer, gyroscope and magnetometer}
We call this group of sensors \textit{I\textsuperscript{2}C sensors}, because they are all connected to the common I\textsuperscript{2}C bus shared between both RPI and STM.

The accelerometer and gyroscope are one-package sensor named LSM330DLC (see \cite{lsm330dlc}). The magnetometer is MAG3110 (see \cite{mag3110}). All of them are 3D.

The supported ranges are: for accelerometer $\mathbf{\pm \SI{2}{g}}$, $\pm \SI{4}{g}$, $\pm \SI{8}{g}$, $\pm \SI{16}{g}$ and for gyroscope it is $\SIlist{ \pm 250 ; \pm 500 ; \pm 2000}{deg\per\second}$. The magnetometer has fixed range and it is $\SI{\pm1.55}{\micro\tesla}$. 