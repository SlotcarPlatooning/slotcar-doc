load('identificationSpeed')
%%
figure(1);
inp01 = get(data, 1);
real01 = get(data, 2);
mod01 = get(data, 3);

time = inp01.Values.Time;
ind = find(time < 7);
time = time(ind);
plot(time, inp01.Values.Data(ind),':', ...
    time, real01.Values.Data(ind),  ...
    time, mod01.Values.Data(ind),'LineWidth', 2);
xlabel('Time [s]')
ylim([-0.1,1.2]);
legend('v_{ref} [-]', 'v - true [m/s]', 'v - model [m/s]', 'Location', 'NorthWest');
SavePlot(1, 'Figures/identSpeed');