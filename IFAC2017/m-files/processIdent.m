load('identification')
%%
figure(1);
inp01 = get(data, 10);
real01 = get(data, 11);
mod01 = get(data, 12);

time = inp01.Values.Time;
ind = find(time < 7);
time = time(ind);
plot(time, inp01.Values.Data(ind), ...
    time, real01.Values.Data(ind), ...
    time, mod01.Values.Data(ind));

figure(2)
inp02 = get(data, 7);
real02 = get(data, 8);
mod02 = get(data, 9);

time = inp02.Values.Time;
ind = find(time < 5);
time = time(ind);
plot(time, inp02.Values.Data(ind), ...
    time, real02.Values.Data(ind), ...
    time, mod02.Values.Data(ind));

figure(3)
inp03 = get(data, 4);
real03 = get(data, 5);
mod03 = get(data, 6);

time = inp03.Values.Time;
ind = find(time < 4.5);
time = time(ind);
plot(time, inp03.Values.Data(ind), ...
    time, real03.Values.Data(ind), ...
    time, mod03.Values.Data(ind));

figure(3)
inp04 = get(data, 1);
real04 = get(data, 2);
mod04 = get(data, 3);

time = inp04.Values.Time;
ind = find((time < 4.0)&&(time > 1));
time = time(ind);
plot(time, inp04.Values.Data(ind), ...
    time, real04.Values.Data(ind), ...
    time, mod04.Values.Data(ind));

%%
figure(5)
time = inp04.Values.Time;
ind = find((time < 4.0));
time = time(ind)-1;
plot(time, inp02.Values.Data(ind), ':b', time, real02.Values.Data(ind),'b', time, mod02.Values.Data(ind), '--b',...
    time, inp04.Values.Data(ind), ':k', time, real04.Values.Data(ind) ,'k', time, mod04.Values.Data(ind), '--k', ...
    'LineWidth', 2);
xlim([0, 3])
xlabel('Time [s]')
legend('duty cycle [-]', 'v - true [m/s]', 'v - model [m/s]', 'Location', 'NorthWest');
SavePlot(5, 'Figures/ident');