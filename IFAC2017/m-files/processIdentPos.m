load('identificationPos3')
%%
figure(1);
dist1 = get(data, 1);
dist2 = get(data, 2);
mod1 = get(data, 3);
mod2 = get(data, 4);

time = dist1.Values.Time;
ind = find(time < 12);
time = time(ind);
plot(time, dist1.Values.Data(ind), 'b',...
    time, dist2.Values.Data(ind), 'r', ...
    time, mod1.Values.Data(ind),'--b',...
    time, mod2.Values.Data(ind),'--r', 'LineWidth', 2);
xlabel('Time [s]')
ylim([-0.05, 0.35])
legend('\Delta_1 - true [m]', '\Delta_2 - true [m]','\Delta_1 - model [m]', '\Delta_2 - model [m]', 'Location', 'NorthEast');
SavePlot(1, 'Figures/identPos');