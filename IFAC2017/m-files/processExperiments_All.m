%% General settings
xlimit = [0, 50];
ylimit = [0.0, 0.40];
fontSize = 20;
N = 5;
legends = {'Ref', 'Car 1', 'Car 2', 'Car 3', 'Car 4'};

%% Leader's vel
figure(6);
load('views/PD_good_2.mat')
desVel = get(data, 1);
velTrue = get(data, 3);
distTrue = get(data, 2);
distSim = get(data,4);
velSim = get(data,5);


plot(desVel.Values.Time-2, desVel.Values.Data, 'LineWidth', 2)
xlim(xlimit)
xlabel('Time [s]');
ylabel('Des. vel. leader [m]');

%% PD controller
figure(1);
load('views/PD_good_2.mat')
desVel = get(data, 1);
velTrue = get(data, 3);
distTrue = get(data, 2);
distSim = get(data,4);
velSim = get(data,5);

time = distTrue.Values.Time-2;
sig = permute(distTrue.Values.Data, [3 1 2]);
plot(time, sig(:,1), '--k', time, sig(:, 2:end), 'LineWidth', 2)
ylim(ylimit)
xlim(xlimit)
xlabel('Time [s]');
ylabel('Distance [m]');
legend(legends, 'Location', 'NorthEast');
SavePlot(1, 'Figures/PD_5cars', fontSize)

%% PD with Feedforward controller
figure(2);
load('views/PD_FF_good_2.mat')
desVel = get(data, 1);
velTrue = get(data, 3);
distTrue = get(data, 2);
distSim = get(data,4);
velSim = get(data,5);

time = distTrue.Values.Time-2;
sig = permute(distTrue.Values.Data, [3 1 2]);
plot(time, sig(:,1), '--k', time, sig(:, 2:end), 'LineWidth', 2)
ylim(ylimit)
xlim(xlimit)
xlabel('Time [s]');
ylabel('Distance [m]');
legend(legends, 'Location', 'NorthEast');
SavePlot(2, 'Figures/PD_FF_5cars', fontSize)

%% PI 
figure(3);
load('views/PI_good_2.mat')
desVel = get(data, 1);
velTrue = get(data, 3);
distTrue = get(data, 2);
distSim = get(data,4);
velSim = get(data,5);

time = distTrue.Values.Time-2;
sig = permute(distTrue.Values.Data, [3 1 2]);
plot(time, sig(:,1), '--k', time, sig(:, 2:end), 'LineWidth', 2)
ylim(ylimit)
xlim(xlimit)
xlabel('Time [s]');
ylabel('Distance [m]');
legend(legends, 'Location', 'NorthEast');
SavePlot(3, 'Figures/PI_5cars', fontSize)

%% PI bidirectional
figure(4);
load('views/PI_bid_good_2.mat')
desVel = get(data, 1);
velTrue = get(data, 3);
distTrue = get(data, 2);
distSim = get(data,4);
velSim = get(data,5);

time = distTrue.Values.Time-2;
sig = permute(distTrue.Values.Data, [3 1 2]);
plot(time, sig(:,1), '--k', time, sig(:, 2:end), 'LineWidth', 2)
hold on;
timeSim = distSim.Values.Time-2;
sigSim = distSim.Values.Data;
axis = gca;
axis.ColorOrderIndex = 1;
plot(timeSim, sigSim, '--', 'LineWidth', 2);
hold off;
ylim(ylimit)
xlim(xlimit)
xlabel('Time [s]');
ylabel('Distance [m]');
legend(legends, 'Location', 'NorthEast');
SavePlot(4, 'Figures/PI_Bid_5cars', 18)

%% PI StringInstab
figure(5);
load('views\string_instab_5_cars_PI_exp1.mat')
d1 = get(data,1);
d2 = get(data,2);
d3 = get(data,3);
d4 = get(data,4);
d5 = get(data,5);

mult = 1.4;
time = d1.Values.Time-1.5;
plot(time, d1.Values.Data*mult, 'k', time, d2.Values.Data*mult, time, d3.Values.Data*mult, ...
       time, d4.Values.Data*mult, time, d5.Values.Data*mult, 'LineWidth', 2)
%ylim(ylimit)
xlim([0 3.5])
ylim([-0.1, 1.8])
xlabel('Time [s]');
ylabel('Velocity [m/s]');
legends{1} = 'Leader'
legend(legends, 'Location', 'NorthWest');
SavePlot(5, 'Figures/PI_StringInstab_5cars', 18)