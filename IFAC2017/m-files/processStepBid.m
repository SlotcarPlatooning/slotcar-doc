

%%
figure(1);
load('stepVelBidPI_N2')
dist1 = get(data, 1);
dist2 = get(data, 2);

time = dist1.Values.Time;
ind = find(time < 12);
time = time(ind);
plot(time, dist1.Values.Data(ind), 'b',...
    time, dist2.Values.Data(ind), 'r', 'LineWidth', 2);
xlabel('Time [s]')
ylim([-0.05, 0.35])
legend('\Delta_1 - true [m]', '\Delta_2 - true [m]', 'Location', 'NorthEast');
grid on;
SavePlot(1, 'Figures/stepVelBidPI');

