

%%
figure(1);
load('stepVelPIInstab_N5')
dist1 = get(data, 1);
dist2 = get(data, 2);
dist3 = get(data, 3);
dist4 = get(data, 4);
dist5 = get(data, 5);

time = dist1.Values.Time;
ind = find(time < 8);
time = time(ind);
plot(time, dist1.Values.Data(ind),...
    time, dist2.Values.Data(ind),...
    time, dist3.Values.Data(ind),...
    time, dist4.Values.Data(ind),...
    time, dist5.Values.Data(ind),...
    'LineWidth', 2);
xlabel('Time [s]')
xlim([1.5, 8])
ylabel('Velocity [m/s]')
legend('v_1', 'v_2', 'v_3', 'v_4', 'v_5','Location', 'NorthEast');
grid on;
SavePlot(1, 'Figures/stepVelPIInstab');

