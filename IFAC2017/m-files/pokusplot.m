clc;close all
load('pokus1.mat')


% Create figure
figure('Color',[1 1 1]);

speed =  ScopeData.signals(1,1).values;
dist = ScopeData.signals(1,2).values;


N1 = 5 
N2 = 5

t_from = 12
t_to  = 18


t =  ScopeData.time;
ind = find(t < t_to & t > t_from);
t = t(ind)
t = t - t_from



% Create axes
axes1 = axes;
hold(axes1,'on');

subplot(2,1,1); 
hold on
for i = 1:N1
    plot(t,shiftdim(speed(i,1,ind)),'LineWidth',2)
end
legend('1','2','3','4','5')
% Create xlabel
xlabel('Time [s]');
hold off


subplot(2,1,2); 
hold on
for i = 1:N2
    plot(t,shiftdim(dist(i,1,ind)),'LineWidth',2)
end

legend('1','2','3','4','5')
% Create xlabel
xlabel('Time [s]');
set(axes1,'FontSize',14);
% Uncomment the following line to preserve the Y-limits of the axes
% ylim(axes1,[-0.05 0.35]);
box(axes1,'on');
grid(axes1,'on');
% Set the remaining axes properties

% Create legend
legend(axes1,'show');
hold offs











%%
load('pokus2.mat')
speed =  ScopeData.signals(1,1).values;
dist = ScopeData.signals(1,2).values;

N1 = 5 
N2 = 5

subplot(2,1,1); 
hold on
for i = 1:N1
    plot(ScopeData.time,shiftdim(speed(i,1,:)))
end
hold off
subplot(2,1,2); 
hold on
for i = 1:N2
    plot(ScopeData.time,shiftdim(dist(i,1,:)))
end