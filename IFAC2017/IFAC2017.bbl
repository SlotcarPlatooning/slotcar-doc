\begin{thebibliography}{14}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi:\discretionary{}{}{}#1}\else
  \providecommand{\doi}{doi:\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi

\bibitem[{Alam et~al.(2015)Alam, M{\aa}rtensson, and Johansson}]{Alam2015}
Alam, A., M{\aa}rtensson, J., and Johansson, K.H. (2015).
\newblock {Experimental evaluation of decentralized cooperative cruise control
  for heavy-duty vehicle platooning}.
\newblock \emph{Control Engineering Practice}, 38, 11--25.

\bibitem[{Barooah and Hespanha(2005)}]{Barooah2005}
Barooah, P. and Hespanha, J.P. (2005).
\newblock {Error Amplification and Disturbance Propagation in Vehicle Strings
  with Decentralized Linear Control}.
\newblock In \emph{Proceedings of the 44th IEEE Conference on Decision and
  Control}, 4964--4969. IEEE.

\bibitem[{Barooah et~al.(2009)Barooah, Mehta, and Hespanha}]{Barooah2009a}
Barooah, P., Mehta, P.G., and Hespanha, J.P. (2009).
\newblock {Mistuning-Based Control Design to Improve Closed-Loop Stability
  Margin of Vehicular Platoons}.
\newblock \emph{IEEE Transactions on Automatic Control}, 54(9), 2100--2113.

\bibitem[{Coelingh and Solyom(2012)}]{Coelingh2012}
Coelingh, E. and Solyom, S. (2012).
\newblock {All aboard the robotic road train}.
\newblock \emph{IEEE Spectrum}, 49(11), 34--39.

\bibitem[{Herman et~al.(2016{\natexlab{a}})Herman, Martinec, Hur{\'{a}}k, and
  Sebek}]{Herman2014b}
Herman, I., Martinec, D., Hur{\'{a}}k, Z., and Sebek, M. (2016{\natexlab{a}}).
\newblock {Scaling in bidirectional platoons with dynamic controllers and
  proportional asymmetry}.
\newblock \emph{IEEE Transactions on Automatic Control}, PP(99), 1--6.

\bibitem[{Herman et~al.(2016{\natexlab{b}})Herman, Martinec, and
  Veerman}]{Herman2016}
Herman, I., Martinec, D., and Veerman, J.J.P. (2016{\natexlab{b}}).
\newblock {Transients of platoons with asymmetric and different Laplacians}.
\newblock \emph{Systems {\&} Control Letters}, 91, 28--35.

\bibitem[{Lin et~al.(2012)Lin, Fardad, Jovanovi{\'{c}}, and
  Jovanovic}]{Lin2012}
Lin, F., Fardad, M., Jovanovi{\'{c}}, M.R., and Jovanovic, M.R. (2012).
\newblock {Optimal control of vehicular formations with nearest neighbor
  interactions}.
\newblock \emph{IEEE Transactions on Automatic Control}, 57(9), 2203--2218.

\bibitem[{Lunze(2012)}]{Lunze2012}
Lunze, J. (2012).
\newblock {Synchronization of Heterogeneous Agents}.
\newblock \emph{IEEE Transactions on Automatic Control}, 57(11), 2885--2890.

\bibitem[{Martinec et~al.(2012)Martinec, Sebek, and
  Hurak}]{martinec_vehicular_2012}
Martinec, D., Sebek, M., and Hurak, Z. (2012).
\newblock Vehicular platooning experiments with racing slot cars.
\newblock In \emph{2012 {IEEE} {International} {Conference} on {Control}
  {Applications} ({CCA})}, 166--171.

\bibitem[{Milanes et~al.(2014)Milanes, Shladover, Spring, Nowakowski, Kawazoe,
  and Nakamura}]{Milanes2014}
Milanes, V., Shladover, S.E., Spring, J., Nowakowski, C., Kawazoe, H., and
  Nakamura, M. (2014).
\newblock {Cooperative adaptive cruise control in real traffic situations}.
\newblock \emph{IEEE Transactions on Inteligent Transportation Systems}, 15(1),
  296--305.

\bibitem[{Naus et~al.(2010)Naus, Vugts, Ploeg, van~de Molengraft, and
  Steinbuch}]{Naus2010}
Naus, G., Vugts, R., Ploeg, J., van~de Molengraft, R., and Steinbuch, M.
  (2010).
\newblock {Cooperative adaptive cruise control, design and experiments}.
\newblock In \emph{American Control Conference 2010}, 1, 6145--6150.

\bibitem[{Seiler et~al.(2004)Seiler, Pant, and Hedrick}]{Seiler2004a}
Seiler, P., Pant, A., and Hedrick, K. (2004).
\newblock {Disturbance Propagation in Vehicle Strings}.
\newblock \emph{IEEE Transactions on Automatic Control}, 49(10), 1835--1841.

\bibitem[{Wieland et~al.(2011)Wieland, Sepulchre, and
  Allg{\"{o}}wer}]{Wieland2011}
Wieland, P., Sepulchre, R., and Allg{\"{o}}wer, F. (2011).
\newblock {An internal model principle is necessary and sufficient for linear
  output synchronization}.
\newblock \emph{Automatica}, 47(5), 1068--1074.

\bibitem[{Yadlapalli et~al.(2006)Yadlapalli, Darbha, and
  Rajagopal}]{Yadlapalli2006}
Yadlapalli, S., Darbha, S., and Rajagopal, K. (2006).
\newblock {Information Flow and Its Relation to Stability of the Motion of
  Vehicles in a Rigid Formation}.
\newblock \emph{IEEE Transactions on Automatic Control}, 51(8), 1315--1319.

\end{thebibliography}
